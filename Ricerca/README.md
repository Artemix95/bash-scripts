# Ricerca all'interno di un file

Questo script prevede la ricerca all'interno di uno specifico file presente sul computer.

Si può eseguire la ricerca inserendo una stringa di caratteri, verranno restituite le frasi
che contengono la stringa esatta (no case sensitive).

Come alternativa si può scegliere di cercare fino a un massimo di tre parole, la ricerca restituirà le frasi che contengono queste
parole, non importa in quale ordine della frase si trovano.
Nei risultati apparirà in rosso la stringa o le parole cercate, e in blu (per esigenze personali) tutte le estensioni
dei file video (no case sensitive).

